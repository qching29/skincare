import React, { Component } from 'react';
import { BrowserRouter, Route, Link } from "react-router-dom";
import "./Articlelist.scss";
import $ from 'jquery';

class Type extends Component{
    constructor(props) {
        super(props)
        console.log(props)
        // this.props=this.params.usid
        this.state = {article :[]}
        this.handleChange = this.handleChange.bind(this)
        // console.log(this.state);
        
        }  
    handleChange() {
        // var b = JSON.parse(localStorage.getItem("member"));
        // fetch("http://localhost:3000/bv/article/my/" +b[0].id)
        // fetch("http://localhost:3000/bv/article/type/" + this.props.match.params.pro_type)
        fetch("http://localhost:3000/bv/article/type/")
        .then(res => res.json())
        .then(articles =>
            this.setState({article:articles}
                ))
        
        // fetch("http://localhost:3000/bv/article/type1/")
        // .then(res => res.json())
        // .then(articles =>
        //     this.setState({article:articles}
        //         ))
        // fetch("http://localhost:3000/bv/article/type2/")
        // .then(res => res.json())
        // .then(articles =>
        //     this.setState({article:articles}
        //         ))
        // fetch("http://localhost:3000/bv/article/type3/")
        // .then(res => res.json())
        // .then(articles =>
        //     this.setState({article:articles}
        //         ))
        // fetch("http://localhost:3000/bv/article/type4/")
        // .then(res => res.json())
        // .then(articles =>
        //     this.setState({article:articles}
        //         ))
        }

    detail = evt => {
        evt.preventDefault()
        var c = (evt.target.dataset.detail);
        console.log(c)
        // /article/articlelist/detail/:id
        fetch("http://localhost:3000/bv/article/articlelist/type/" + c)
        // fetch( c)
            .then(res => res.json())
                    .then(data => {
                        alert(data.message);
                    })
        }
    
    

        

    //網頁產生後會觸發此事件  
    componentDidMount() {
        this.handleChange();
    }
    render() {
        // console.log(this.state)
        return (
            <React.Fragment>
            <div className="container">
                <div className="row">
                
                    {this.state.article.map(article =>
                            <div key={article.id} className="col-12 col-sm-6 col-lg-3 mt-5">
                                {/* style={{border:" 1px solid black"}} */}
                             
                                <div className="card-h bordercolor ">
                                
                                <figure className="fig-h text-center">
                            
                                <img className="pic-h " src = {(`http://localhost:3000/bv/pro_pic/${article.pro_picnum}`)} alt=""/>
                                {/* src = {require(`../../liliimages/${article.pro_picnum}.jpg`)} */}
                                </figure>
                                    <div className="text-left">
                                    <h5 className=" d-inline-flex  textblack pl-2">[{article.pre_title}]</h5> 
                                    <h5 className="  d-inline-flex   textblack text-truncate">{article.title}</h5>
                                    </div>
                                 
                                    <p className=" textblack text-truncate pl-2">{article.text}</p>
                                    <div className="text-left">
                                    <p className=" textblack pl-2">類型：<a href="#" className="textblack">#{article.pro_type}</a>
                                        <a href="#" className=" textblack">#{article.pro_type}</a>
                                    </p>
                                    </div> 
                                    {/* 星星變數亮燈燈*/}
                                    <div className="text-left ml-2">
                                    <i className="far fa-star "></i> 
                                    <i className="far fa-star "></i>
                                    <i className="far fa-star "></i>
                                    <i className="far fa-star "></i>
                                    <i className="far fa-star "></i>
                                    <p className="d-inline-flex textblack">({article.star})</p>
                                    </div> 
                                    {/* 星星變數亮燈燈*/}
                                    
                                        <div className="row justify-content-between">
                                        <div className="col text-left ml-2">
                                        <i className="far fa-heart  textblack "/>
                                        <p className="d-inline-flex  textblack ml-1">{article.collect}</p>
                                       
                                       
                                        <i className="far fa-comment  textblack ml-2"/>
                                        <p className="d-inline-flex textblack ">{article.count}</p>
                                        </div>
                                       
                                        <div className="col text-right ">
                                        {/* <p className="d-inline-flex text-right textprimary font-weight-bold pr-2">觀看更多</p> */}
                                        <Link to={`${this.props.match.url}${article.id}`} data-detail={article.id} onclick={this.detail}><p className="d-inline-flex text-right textprimary font-weight-bold pr-2">觀看更多</p></Link>
                                        </div>
                                        </div>
                                
                                    
                                    {/* <p className="justify-content-end">{article.created_at}</p> */}
                                 
                                </div>
                                </div>
                    )}
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default Type;